<?php
/**
 * @file
 * Defines the inequality operator for use in static and dynamic filters.
 */
class MVInequalityOperator extends MVOperator {
  protected $comparison_value = NULL;

  public function __construct($value = NULL) {
    $this->comparison_value = $value;
  }

  public function apply($value) {
    return $value != $this->comparison_value;
  }

  public function endsIndex() {
    return TRUE;
  }

  public function indexable() {
    return TRUE;
  }
}