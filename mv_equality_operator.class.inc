<?php
/**
 * @file
 * Defines the equality operator for use in static and dynamic filters.
 */
class MVEqualityOperator extends MVOperator {
  protected $comparison_value = NULL;

  public function __construct($value = NULL) {
    $this->comparison_value = $value;
  }

  public function apply($value) {
    return $value == $this->comparison_value;
  }

  public function endsIndex() {
    return FALSE;
  }

  public function indexable() {
    return TRUE;
  }
}