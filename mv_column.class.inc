<?php
/**
 *
 * @file
 * Defines a column data source for use in materialized views.
 *
 * Subclass to define usable data sources.
 */
abstract class MVColumn {
  /**
   * Retreive the value to use for this column, given an entity typ[e
   * and entity ID.
   *
   * @param $entity_type
   *   The entity type.
   * @param $entity_id
   *   The entity ID.
   */
  abstract public function getValue($entity_type, $entity_id);

  /**
   * For the given entity type and ID arguments, return an array
   * identifying types and IDs affected by the change. The default
   * implementation below assumes the this column object does not
   * aggregate data from other object types.
   *
   * A key example of why this function would be implemented differently
   * is the node to comment relationship for a column calculating
   * the timestamp of the most recent comment on a node. While
   * getValue() would only be called for nodes, creating of editing a comment
   * would potentially affect the value for each associated node. Thus,
   * an "timestamp of the most recent comment on a node" subclass
   * of MVColumn would return the associated node IDs for a given
   * comment and an empty array for any other entity type.
   *
   * @param $entity_type
   *   The entity type.
   * @param $entity_id
   *   The entity ID.
   *
   * @returns Mapping of types and IDs affected.
   */
  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();
    $changed[$entity_type] = array($entity_id);
    return $changed;
  }

  /**
   * Retrieve a column scheme suitable for use in $table['fields'].
   *
   * @returns The column scheme.
   */
  abstract public function getSchema();

  /**
   * Retrieve the column name.
   *
   * @returns A column name.
   */
  abstract public function getName();
}
