<?php
/**
 * @file
 *
 * This file defines the MV Entity Cache class
 * MV Entity Cache Class
 */
class MVEntityCache {
  protected static $entities = array();

  public static function set($entity_type, $entity_id, $entity) {
    if (!isset(self::$entities[$entity_type])) {
      self::$entities[$entity_type] = array();
    }

    self::$entities[$entity_type][$entity_id] = $entity;
  }

  public static function get($entity_type, $entity_id, $autoload = TRUE) {
    if (isset(self::$entities[$entity_type][$entity_id])) {
      return self::$entities[$entity_type][$entity_id];
    }

    if ($autoload === FALSE) {
      return FALSE;
    }

    $load_function = $autoload;
    if ($autoload === TRUE) {
      $load_function = $entity_type . '_load';
    }
    self::$entities[$entity_type][$entity_id] = $load_function($entity_id);
    return self::$entities[$entity_type][$entity_id];
  }
}