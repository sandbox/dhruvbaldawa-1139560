<?php
/**
 * @file
 * This class manages the hook_exit() indexing queue.
 */
final class MVJobQueue {
  static $update_jobs = array();
  static $deletion_jobs = array();

  /**
   * Update all materialized views for the given object.
   *
   * @param $entity
   *   The entity type ID.
   * @param $entity_id
   *   The entity ID.
   */
  public static function update($entity_type, $entity_id) {
    $job = new stdClass();
    $job->entity_type = $entity_type;
    $job->entity_id = $entity_id;
    self::$update_jobs[] = $job;
  }

  /**
   * Delete the given object from all materialized views.
   *
   * @param $entity
   *   The entity type ID.
   * @param $entity_id
   *   The entity ID.
   */
  public static function delete($entity_type, $entity_id) {
    $job = new stdClass();
    $job->entity_type = $entity_type;
    $job->entity_id = $entity_id;
    self::$deletion_jobs[] = $job;
  }

  public static function run() {
    // Don't do anything if no jobs are queued.
    if (empty(self::$update_jobs) && empty(self::$deletion_jobs)) {
      return;
    }

    // Ensure the MV schema are up to date.
    materialized_view_reconcile();

    // Load the materialized views.
    $materialized_views = materialized_view_get();

    // Run update jobs.
    foreach (self::$update_jobs as $job) {
      foreach ($materialized_views as $materialized_view) {
        $materialized_view->update($job->entity_type, $job->entity_id);
      }
    }

    // Run deletion jobs.
    foreach (self::$deletion_jobs as $job) {
      foreach ($materialized_views as $materialized_view) {
        $materialized_view->delete($job->entity_type, $job->entity_id);
      }
    }
  }
}